#!/bin/bash -ex

source ${DIR_SCRIPTS_PATH}/lkft-common-bash-function.sh
###########################################################
kernel_branch="${1}"

kernel_version=$(get_kernel_version)
kernel_label="${kernel_version}-${CI_COMMIT_SHA:0:12}"

curl -fsSL https://raw.githubusercontent.com/Linaro/android-report/master/deploy.sh -o deploy.sh && chmod +x deploy.sh

mkdir -p /android/django_instances
./deploy.sh /android/django_instances false
f_report="/tmp/kernelreport-${kernel_branch}-${kernel_label}.txt"
for i in {1..5}; do
    echo "Try for the $i times to generate the report"
    /android/django_instances/android-report/kernelreport.sh "${kernel_branch}" --exact-version-1 "${kernel_label}" --no-check-kernel-version
    if [ -f "${f_report}" ]; then
        break
    fi
done

if [ ! -f "${f_report}" ]; then
    echo "Tried to generate the report for kernel: ${kernel_branch} ${kernel_label} but failed" > ${f_report}
fi

msg_subject="[lkft-android-report] ${CI_PROJECT_NAME} | ${kernel_branch} | ${kernel_label}"
send_mail_common "${f_report}" "${msg_subject}"
