#!/bin/bash -ex

source ${DIR_SCRIPTS_PATH}/lkft-common-bash-function.sh
##########################################################
# https://www.jenkins.io/doc/book/using/remote-access-api/
# https://beginnersforum.net/blog/2019/11/28/jenkin-paramerized-job-api-json/
# https://ci.linaro.org/user/yongqin.liu@linaro.org/configure

if [ -z "${JENKINS_TOKEN}" ] || [ -z "${JENKINS_USERNAME}" ]; then
    echo "Please specify JENKINS_USERNAME and JENKINS_TOKEN to start jenkins job"
    exit 1
fi

kernel_version=$(get_kernel_version)

BUILD_DIR="lkft"
LKFT_ANDROID_BUILD_CONFIG="${LKFT_ANDROID_BUILD_CONFIG:-lkft-gki-android-mainline}"
JENKINS_CI_BUILD_NAME="${JENKINS_CI_BUILD_NAME:-trigger-hikey-stable}"
JENKINS_CALL_URL="https://ci.linaro.org/job/${JENKINS_CI_BUILD_NAME}/buildWithParameters"
JENKINS_JOB_URL="https://ci.linaro.org/job/${JENKINS_CI_BUILD_NAME}"

KERNEL_BRANCH="${CI_BUILD_REF_NAME:-unknown}"
KERNEL_COMMIT="${CI_COMMIT_SHA:-unknown}"
KERNEL_VERSION="${kernel_version:-unknown}"
KERNEL_DESCRIBE="${KERNEL_VERSION}-${KERNEL_COMMIT:0:12}"
export KERNEL_LABEL="${KERNEL_DESCRIBE}"

BUILD_REFERENCE_IMAGE_GZ_URL=""
if [ -n "${LKFT_ANDROID_BUILD_GKI_CONFIG}" ]; then
    f_gki_build_json="${LKFT_ANDROID_BUILD_GKI_CONFIG}-build.json"
    snapshot_download_url=""
    if [ -f "${f_gki_build_json}" ]; then
        snapshot_download_url=$(jq .SNAPSHOT_DOWNLOAD_URL "${f_gki_build_json}" |tr -d \")
    fi
    if [ -n "${snapshot_download_url}" ]; then
        BUILD_REFERENCE_IMAGE_GZ_URL="${snapshot_download_url}/${LKFT_ANDROID_BUILD_GKI_CONFIG}-Image.gz"
    fi
fi

#--form FILE_LOCATION_AS_SET_IN_JENKINS=@PATH_TO_FILE
f_jenkins_header="${LKFT_ANDROID_BUILD_CONFIG}-jenkins-response-header.txt"
curl ${JENKINS_CALL_URL} \
    --user ${JENKINS_USERNAME}:${JENKINS_TOKEN} \
    --data BUILD_DIR=${BUILD_DIR} \
    --data ANDROID_BUILD_CONFIG="${LKFT_ANDROID_BUILD_CONFIG}" \
    --data KERNEL_DESCRIBE="${KERNEL_DESCRIBE}" \
    --data KERNEL_BRANCH="${KERNEL_BRANCH}" \
    --data SRCREV_kernel="${KERNEL_COMMIT}" \
    --data MAKE_KERNELVERSION="${KERNEL_VERSION}" \
    --data QA_BUILD_VERSION="${KERNEL_LABEL}" \
    --data BUILD_REFERENCE_IMAGE_GZ_URL="${BUILD_REFERENCE_IMAGE_GZ_URL}" \
    --dump-header ${f_jenkins_header}

if [ $? -ne 0 ]; then
    echo "Failed to trigger jenkins build for ${CI_PROJECT_NAME} | ${CI_BUILD_REF_NAME} | ${KERNEL_LABEL}: ${LKFT_ANDROID_BUILD_CONFIG}"
    exit 1
fi

## https://stackoverflow.com/questions/24507262/retrieve-id-of-remotely-triggered-jenkins-job/28524219#28524219
## find the job url in the queue
url_jenkins_queue=$(grep 'Location:' ${f_jenkins_header} |cut -d\  -f2 | tr -d '\n\r')
if [ -z "${url_jenkins_queue}" ]; then
    echo "Failed to get the queue job url after the job submitted"
    exit 1
fi

url_jenkins_queue=$(echo ${url_jenkins_queue} |tr -s '\/'|sed 's|\/$||')
jenkins_queueid=${url_jenkins_queue##*/}
if [ -z "${jenkins_queueid}" ]; then
    echo "Faild to get the jenkins queue id for the job triggered"
    exit 1
fi

#--form FILE_LOCATION_AS_SET_IN_JENKINS=@PATH_TO_FILE
jenkins_build_number=""
jenkins_build_url=""
JENKINS_JOB_URL="https://ci.linaro.org/job/${JENKINS_CI_BUILD_NAME}"
f_jenkins_number_queueid="${LKFT_ANDROID_BUILD_CONFIG}-jenkins-number-queueid.json"
f_jenkins_queue_json="${LKFT_ANDROID_BUILD_CONFIG}-jenkins-queue.json"
JENKINS_CALL_URL="${JENKINS_JOB_URL}/api/json?tree=builds\[number,queueId,building,result,timestamp,duration\]"

job_in_queue=true
job_in_progress=false
job_finished=false
queue_job_cancelled=false
queue_job_not_found=false
while true; do
    sleep 120

    if ${job_in_queue}; then
        url_jenkins_queue_api="${url_jenkins_queue}/api/json"
        curl -L "${url_jenkins_queue_api}" \
            --user ${JENKINS_USERNAME}:${JENKINS_TOKEN} \
            -o "${f_jenkins_queue_json}"

        if grep -i '404 Not Found' "${f_jenkins_queue_json}"; then
            queue_job_not_found=true
        else
            #queue_job_blocked=$(jq .blocked "${f_jenkins_queue_json}")
            queue_job_cancelled=$(jq .cancelled "${f_jenkins_queue_json}")
            if [ "X${queue_job_cancelled}" = "Xtrue" ]; then
                echo "The queue job is cancelled"
                break
            fi
        fi
    fi

    curl -L ${JENKINS_CALL_URL} \
        --user ${JENKINS_USERNAME}:${JENKINS_TOKEN} \
        -o "${f_jenkins_number_queueid}"
    jenkins_build_number=$(jq -r ".builds[] |select(.queueId == ${jenkins_queueid})| .number" "${f_jenkins_number_queueid}")
    if [ -z "${jenkins_build_number}" ]; then
        if ${queue_job_not_found}; then
            echo "The build triggered is still in queue: ${url_jenkins_queue}"
            break
        else
            echo "The build triggered is still in queue: ${url_jenkins_queue}"
            continue
        fi
    fi

    jenkins_build_url="${JENKINS_JOB_URL}/${jenkins_build_number}"
    if ${job_in_queue}; then
        echo "Please check here for the jenkins job: ${jenkins_build_url}"
        job_in_queue=false
        job_in_progress=true
        continue
    fi

    building=$(jq -r ".builds[] |select(.queueId == ${jenkins_queueid})| .building" ${f_jenkins_number_queueid})
    result=$(jq -r ".builds[] |select(.queueId == ${jenkins_queueid})| .result" ${f_jenkins_number_queueid})
    if ! ${building}; then
        echo "The jenkins job is finished now: ${jenkins_build_url} ${result}"
        job_in_progress=false
        job_finished=true
        break
    fi

done

SNAPSHOT_DOWNLOAD_URL=""
if [ -n "${jenkins_build_number}" ]; then
    PUB_DEST="android/lkft/${JENKINS_CI_BUILD_NAME}/${jenkins_build_number}"
    SNAPSHOT_DOWNLOAD_URL="http://snapshots.linaro.org/${PUB_DEST}"
fi

f_build_json="${LKFT_ANDROID_BUILD_CONFIG}-build.json"
cat >"${f_build_json}" <<__EOF__
{
    "JENKINS_BUILD_URL": "${jenkins_build_url}",
    "JENKINS_BUILD_NUMBER": "${jenkins_build_number}",
    "JENKINS_JOB_URL": "${JENKINS_JOB_URL}",
    "JENKINS_CI_BUILD_NAME": "${JENKINS_CI_BUILD_NAME}",
    "SNAPSHOT_DOWNLOAD_URL": "${SNAPSHOT_DOWNLOAD_URL}",
    "ANDROID_BUILD_CONFIG_TO_BE_TRIGGERED": "${ANDROID_BUILD_CONFIG_TO_BE_TRIGGERED}",
    "ANDROID_BUILD_CONFIG": "${LKFT_ANDROID_BUILD_CONFIG}",
    "KERNEL_DESCRIBE": "${KERNEL_DESCRIBE}",
    "KERNEL_BRANCH": "${KERNEL_BRANCH}",
    "SRCREV_kernel": "${KERNEL_COMMIT}",
    "MAKE_KERNELVERSION": "${KERNEL_VERSION}",
    "TEST_OTHER_PLANS_OVERRIDE": "${TEST_OTHER_PLANS_OVERRIDE}"
}
__EOF__


if ${job_finished}; then
    curl -L "${jenkins_build_url}/api/json" -o "${LKFT_ANDROID_BUILD_CONFIG}-build-details.json"

    if [ "X${result}" = "XSUCCESS" ]; then
        echo "The job is finished successully: ${jenkins_build_url}"
        # for other cases, mark this build job as FAILED,
        # and the jobs depends on this one won't get run
        exit 0
    else
        echo "The job is finished unsuccessfully: ${jenkins_build_url} ${result}"
        exit 1
    fi
elif ${queue_job_cancelled}; then
    echo "The job is cancelled: ${jenkins_build_url}"
else
    echo "The job status is unknown: ${jenkins_build_url}"
fi

exit 1

### Can not wait the jenkins job to callback gitlab,
### As the jenkins job might be cancelled when it's still in queue
