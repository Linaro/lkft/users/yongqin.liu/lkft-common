#!/bin/bash -ex

lkft_build_config="${1}"
url_lkft_build_config="https://android-git.linaro.org/android-build-configs.git/plain/lkft/${lkft_build_config}?h=lkft"

f_build_json="${lkft_build_config}-build.json"
if [ -n "${KERNEL_BUILD_CONFIG}" ]; then
    f_build_json="${KERNEL_BUILD_CONFIG}-build.json"
fi

if [ ! -f "${f_build_json}" ]; then
    echo "The ${f_build_json} does not exist, and we could not found the url to download kernel files"
    exit 1
fi

if [ -z "${BUILD_TYPE}" ]; then
    BUILD_TYPE="tuxsuite"
fi

if [ "X${BUILD_TYPE}" != "Xtuxsuite" ] && [ "X${BUILD_TYPE}" != "Xjenkins" ]; then
    echo "Unsported BUILD_TYPE specified: ${BUILD_TYPE}"
    exit 1
fi

if [ "X${BUILD_TYPE}" = "Xtuxsuite" ]; then
    KERNEL_VERSION="$(jq -r '.[0].kernel_version' ${f_build_json})"
    SRCREV_kernel="$(jq -r '.[0].git_sha' ${f_build_json})"
elif [ "X${BUILD_TYPE}" = "Xjenkins" ]; then
    KERNEL_VERSION=$(jq .MAKE_KERNELVERSION "${f_build_json}" |tr -d \")
    SRCREV_kernel=$(jq .SRCREV_kernel "${f_build_json}" |tr -d \")
else
    echo "Unsported BUILD_TYPE specified: ${BUILD_TYPE}"
    exit 1
fi


rm -fr ${lkft_build_config}
curl -fsSL ${url_lkft_build_config} -o ${lkft_build_config}
source ${lkft_build_config}

# TODO: has problem for multiple projects case
QA_PROJECT_GROUP="${TEST_QA_SERVER_TEAM}"
QA_PROJECT_NAME="${TEST_QA_SERVER_PROJECT}"

# TODO need to be dynamic for different projects
# need to use the team + project as the callback job name
report_job_name="callback-for-squad-${lkft_build_config}"

# Find report job
echo "Will do tests. Need to register QA callback."
# https://docs.gitlab.com/ee/api/jobs.html
# when all the callback jobs finished, there will be no jobs returned with "scope[]=manual" specified
# scope is the status, will be changed when the job is running or finished, etc
# here does not support the scope to have all jobs returned, and the callback
# could be registered again, even it's called before
curl -f --silent "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/jobs" > ${lkft_build_config}-jobs-manual.json
job_id=$(jq -r ".[] | select(.name == \"${report_job_name}\") | .id" ${lkft_build_config}-jobs-manual.json)
callback_url="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/jobs/${job_id}/play"
echo "Callback URL: [${callback_url}]"

# Registr callback with SQUAD

qa_build_version="${KERNEL_VERSION}-${SRCREV_kernel:0:12}"
project_fullname="${QA_PROJECT_GROUP}/${QA_PROJECT_NAME}"
query_qabuild_url="https://qa-reports.linaro.org/api/builds/?version=${qa_build_version}&project__slug=${QA_PROJECT_NAME}&project__full_name=${project_fullname}"

curl -f --silent -L "${query_qabuild_url}" -o ${lkft_build_config}-qa_build.json
build_id="$(jq -r '.results[0].id' ${lkft_build_config}-qa_build.json)"
curl --silent \
  -X POST "${QA_SERVER}/api/builds/${build_id}/callbacks/" \
  -H "Authorization: Token ${QA_REPORTS_TOKEN}" \
  -F "callback_url=${callback_url}" \
  -F "callback_record_response=true"
