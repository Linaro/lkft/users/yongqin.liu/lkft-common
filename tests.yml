test-hikey:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: hi6220-hikey
    LAVA_TESTS: "${default_lava_tests} --test-case kvm-unit-tests.yaml network-basic-tests.yaml fwts.yaml packetdrill.yaml"
  needs:
    - build-arm64-gcc-11
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.4.y"'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-qemu-arm64:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: qemu_arm64
    LAVA_TESTS: "${default_lava_tests} --test-case packetdrill.yaml"
  needs:
    - build-arm64-gcc-11
    - job: download_prerequisites
      artifacts: true

test-qemu-arm64-debug:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: qemu_arm64
    LAVA_TESTS: "${default_lava_tests}"
    QA_ENVIRONMENT: "qemu-arm64-debug"
  needs:
    - build-arm64-gcc-11-debug
    - job: download_prerequisites
      artifacts: true

test-qemu-arm64-kasan:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: qemu_arm64
    LAVA_TESTS: "--test-plan lkft-ltp"
    QA_ENVIRONMENT: "qemu-arm64-kasan"
  needs:
    - build-arm64-gcc-11-kasan
    - job: download_prerequisites
      artifacts: true

test-qemu-arm64-v8-features:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: qemu_arm64
    LAVA_TESTS: "--test-plan lkft-ltp"
    QA_ENVIRONMENT: "qemu-arm64-armv8-features"
  needs:
    - build-arm64-gcc-11-armv8-features
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '($CI_COMMIT_BRANCH == "master" && $CI_PROJECT_NAME == "linux-next")'
      when: always
    - when: never

test-x15-kasan:
  extends:
    - .test
  variables:
    ARCH: arm
    DEVICE_TYPE: x15
    LAVA_TESTS: "--test-plan lkft-ltp"
    QA_ENVIRONMENT: "x15-kasan"
  needs:
    - build-arm-gcc-11-kasan
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_PROJECT_NAME != "linux-next"'
      when: never
    - if: '($CI_COMMIT_BRANCH == "master" && $CI_PROJECT_NAME == "linux-next")'
      when: on_success
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: never

test-juno-kasan:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: juno-r2
    LAVA_TESTS: "--test-plan lkft-ltp"
    QA_ENVIRONMENT: "juno-r2-kasan"
  needs:
    - build-arm64-gcc-11-kasan
    - job: download_prerequisites
      artifacts: true

test-juno-64k_page_size:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: juno-r2
    LAVA_TESTS: "--test-plan lkft-ltp"
    QA_ENVIRONMENT: "juno-64k_page_size"
  needs:
    - build-arm64-gcc-11-64k_page_size
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.19.y"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-5.4.y"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-5.10.y"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "master"'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-ls2088a-64k_page_size:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: nxp-ls2088
    LAVA_TESTS: "--test-plan lkft-ltp"
    LAVA_SERVER: "https://lavalab.nxp.com/RPC2/"
    QA_ENVIRONMENT: "nxp-ls2088-64k_page_size"
  needs:
    - build-arm64-gcc-11-64k_page_size
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.4.y"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.9.y"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.14.y"'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-db410c:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: dragonboard-410c
    LAVA_TESTS: "${default_lava_tests} --test-case kvm-unit-tests.yaml packetdrill.yaml"
  needs:
    - build-arm64-gcc-11
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.4.y"'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-db410c-igt:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: dragonboard-410c
    LAVA_TESTS: "--test-case igt-gpu-tools.yaml"
    LAVA_TAGS: "production, chamelium"
  needs:
    - build-arm64-gcc-11
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.4.y"'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-db845c:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: dragonboard-845c
    LAVA_TESTS: "${default_lava_tests} --test-case kvm-unit-tests.yaml"
    LAVA_TAGS: "production, lts"
  needs:
    - build-arm64-gcc-11
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^linux-4.*/'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-5.4.y"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-5.10.y"'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-ls2088a:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: nxp-ls2088
    LAVA_TESTS: "${default_lava_tests} --test-case kvm-unit-tests.yaml network-basic-tests.yaml packetdrill.yaml"
    LAVA_SERVER: "https://lavalab.nxp.com/RPC2/"
  needs:
    - build-arm64-gcc-11
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.4.y"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.9.y"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.14.y"'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-ls2088a-fwts:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: nxp-ls2088
    LAVA_TESTS: "--test-case fwts.yaml"
    LAVA_SERVER: "https://lavalab.nxp.com/RPC2/"
  needs:
    - build-arm64-gcc-11
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.4.y"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.9.y"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.14.y"'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-juno:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: juno-r2
    LAVA_TESTS: "${default_lava_tests} --test-case kvm-unit-tests.yaml ltp-open-posix.yaml network-basic-tests.yaml packetdrill.yaml"
  needs:
    - build-arm64-gcc-11
    - job: download_prerequisites
      artifacts: true

test-qemu-arm64-compat:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: qemu_arm64
    LAVA_TESTS: "--test-case ltp-syscalls.yaml"
    ROOTFS_MACHINE: "am57xx-evm"
    QA_ENVIRONMENT: "qemu_arm64-compat"
  needs:
    - build-arm64-gcc-11
    - job: download_prerequisites
      artifacts: true

test-qemu-x86_64-compat:
  extends:
    - .test
  variables:
    ARCH: x86
    DEVICE_TYPE: qemu_x86_64
    LAVA_TESTS: "--test-case ltp-syscalls.yaml"
    ROOTFS_MACHINE: "intel-core2-32"
    QA_ENVIRONMENT: "qemu_x86_64-compat"
  needs:
    - build-x86-gcc-11
    - job: download_prerequisites
      artifacts: true

test-juno-compat:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: juno-r2
    LAVA_TESTS: "--test-case ltp-syscalls.yaml"
    ROOTFS_MACHINE: "am57xx-evm"
    QA_ENVIRONMENT: "juno-r2-compat"
  needs:
    - build-arm64-gcc-11
    - job: download_prerequisites
      artifacts: true

test-x15:
  extends:
    - .test
  variables:
    ARCH: arm
    DEVICE_TYPE: x15
    LAVA_TESTS: "${default_lava_tests} --test-case ltp-open-posix.yaml network-basic-tests.yaml ssuite.yaml"
  needs:
    - build-arm-gcc-11
    - job: download_prerequisites
      artifacts: true

test-x15-igt:
  extends:
    - .test
  variables:
    ARCH: arm
    DEVICE_TYPE: x15
    LAVA_TESTS: "--test-case igt-gpu-tools.yaml"
    LAVA_TAGS: "production, chamelium"
  needs:
    - build-arm-gcc-11
    - job: download_prerequisites
      artifacts: true

test-qemu-arm:
  extends:
    - .test
  variables:
    ARCH: arm
    DEVICE_TYPE: qemu_arm
    LAVA_TAGS: "qemu-arm-32,"
    LAVA_TESTS: "${default_lava_tests}"
  needs:
    - build-arm-gcc-11
    - job: download_prerequisites
      artifacts: true

test-qemu-arm-debug:
  extends:
    - .test
  variables:
    ARCH: arm
    DEVICE_TYPE: qemu_arm
    LAVA_TAGS: "qemu-arm-32,"
    LAVA_TESTS: "${default_lava_tests}"
    QA_ENVIRONMENT: "qemu-arm-debug"
  needs:
    - build-arm-gcc-11-debug
    - job: download_prerequisites
      artifacts: true

test-qemu-arm-kasan:
  extends:
    - .test
  variables:
    ARCH: arm
    DEVICE_TYPE: qemu_arm
    LAVA_TAGS: "qemu-arm-32,"
    LAVA_TESTS: "--test-plan lkft-ltp"
    QA_ENVIRONMENT: "qemu-arm-kasan"
  needs:
    - build-arm-gcc-11-kasan
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_PROJECT_NAME != "linux-next"'
      when: never
    - if: '($CI_COMMIT_BRANCH == "master" && $CI_PROJECT_NAME == "linux-next")'
      when: on_success
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: never

test-x86_64:
  extends:
    - .test
  variables:
    ARCH: x86
    DEVICE_TYPE: x86
    LAVA_TESTS: "${default_lava_tests} --test-case kvm-unit-tests.yaml ltp-open-posix.yaml network-basic-tests.yaml packetdrill.yaml ssuite.yaml"
    #explicit:
    #ROOTFS_MACHINE: "intel-corei7-64"
  needs:
    - build-x86-gcc-11
    - job: download_prerequisites
      artifacts: true

test-x86_64-fwts:
  extends:
    - .test
  variables:
    ARCH: x86
    DEVICE_TYPE: x86
    LAVA_TESTS: "--test-case fwts.yaml"
  needs:
    - build-x86-gcc-11
    - job: download_prerequisites
      artifacts: true

test-qemu-x86_64:
  extends:
    - .test
  variables:
    ARCH: x86
    DEVICE_TYPE: qemu_x86_64
    LAVA_TESTS: "${default_lava_tests} --test-case packetdrill.yaml"
  needs:
    - build-x86-gcc-11
    - job: download_prerequisites
      artifacts: true

test-qemu-x86_64-debug:
  extends:
    - .test
  variables:
    ARCH: x86
    DEVICE_TYPE: qemu_x86_64
    LAVA_TESTS: "${default_lava_tests}"
    QA_ENVIRONMENT: "qemu-x86_64-debug"
  needs:
    - build-x86-gcc-11-debug
    - job: download_prerequisites
      artifacts: true

test-qemu-x86_64-kcsan:
  extends:
    - .test
  variables:
    ARCH: x86
    DEVICE_TYPE: qemu_x86_64
    LAVA_TESTS: "--test-plan lkft-ltp"
    QA_ENVIRONMENT: "qemu-x86_64-kcsan"
  needs:
    - build-x86-clang-12-kcsan
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^linux-4.*/'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-x86_64-kasan:
  extends:
    - .test
  variables:
    ARCH: x86
    DEVICE_TYPE: x86
    LAVA_TESTS: "--test-plan lkft-ltp"
    QA_ENVIRONMENT: "x86-kasan"
  needs:
    - build-x86-gcc-11-kasan
    - job: download_prerequisites
      artifacts: true

test-qemu-x86_64-kasan:
  extends:
    - .test
  variables:
    ARCH: x86
    DEVICE_TYPE: qemu_x86_64
    LAVA_TESTS: "--test-plan lkft-ltp"
    QA_ENVIRONMENT: "qemu-x86_64-kasan"
  needs:
    - build-x86-gcc-11-kasan
    - job: download_prerequisites
      artifacts: true

test-i386:
  extends:
    - .test
  variables:
    ARCH: i386
    DEVICE_TYPE: i386
    LAVA_TESTS: "${default_lava_tests} --test-case ltp-open-posix.yaml network-basic-tests.yaml"
  needs:
    - build-i386-gcc-11
    - job: download_prerequisites
      artifacts: true

test-qemu-i386:
  extends:
    - .test
  variables:
    ARCH: i386
    DEVICE_TYPE: qemu_i386
    LAVA_TESTS: "${default_lava_tests}"
  needs:
    - build-i386-gcc-11
    - job: download_prerequisites
      artifacts: true

test-qemu-i386-debug:
  extends:
    - .test
  variables:
    ARCH: i386
    DEVICE_TYPE: qemu_i386
    LAVA_TESTS: "${default_lava_tests}"
    QA_ENVIRONMENT: "qemu-i386-debug"
  needs:
    - build-i386-gcc-11-debug
    - job: download_prerequisites
      artifacts: true

test-qemu-arm64-rcutorture:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: qemu_arm64
    LAVA_TESTS: "--test-case rcutorture.yaml"
  needs:
    - build-arm64-gcc-11-rcutorture
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^linux-4.4/'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^linux-4.9/'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-qemu-arm-rcutorture:
  extends:
    - .test
  variables:
    ARCH: arm
    DEVICE_TYPE: qemu_arm
    LAVA_TAGS: "qemu-arm-32,"
    LAVA_TESTS: "--test-case rcutorture.yaml"
  needs:
    - build-arm-gcc-11-rcutorture
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^linux-4.4/'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^linux-4.9/'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-qemu-x86_64-rcutorture:
  extends:
    - .test
  variables:
    ARCH: x86
    DEVICE_TYPE: qemu_x86_64
    LAVA_TESTS: "--test-case rcutorture.yaml"
  needs:
    - build-x86-gcc-11-rcutorture
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^linux-4.4/'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^linux-4.9/'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-qemu-i386-rcutorture:
  extends:
    - .test
  variables:
    ARCH: i386
    DEVICE_TYPE: qemu_i386
    LAVA_TESTS: "--test-case rcutorture.yaml"
  needs:
    - build-i386-gcc-11-rcutorture
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^linux-4.4/'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^linux-4.9/'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-qemu-arm64-kunit:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: qemu_arm64
    LAVA_TESTS: "--test-case kunit.yaml"
  needs:
    - build-arm64-gcc-11-kunit
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^linux-4.*/'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-5.4.y"'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-qemu-arm-kunit:
  extends:
    - .test
  variables:
    ARCH: arm
    DEVICE_TYPE: qemu_arm
    LAVA_TAGS: "qemu-arm-32,"
    LAVA_TESTS: "--test-case kunit.yaml"
  needs:
    - build-arm-gcc-11-kunit
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^linux-4.*/'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-5.4.y"'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-qemu-x86_64-kunit:
  extends:
    - .test
  variables:
    ARCH: x86
    DEVICE_TYPE: qemu_x86_64
    LAVA_TESTS: "--test-case kunit.yaml"
  needs:
    - build-x86-gcc-11-kunit
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^linux-4.*/'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-5.4.y"'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-qemu-i386-kunit:
  extends:
    - .test
  variables:
    ARCH: i386
    DEVICE_TYPE: qemu_i386
    LAVA_TESTS: "--test-case kunit.yaml"
  needs:
    - build-i386-gcc-11-kunit
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^linux-4.*/'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-5.4.y"'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-qemu-arm64-clang-12:
  extends:
    - .test
  variables:
    ARCH: arm64
    DEVICE_TYPE: qemu_arm64
    LAVA_TESTS: "--test-plan lkft-ltp"
    QA_ENVIRONMENT: "qemu-arm64-clang"
  needs:
    - build-arm64-clang-12
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.4.y"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.9.y"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.14.y"'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-qemu-arm-clang-12:
  extends:
    - .test
  variables:
    ARCH: arm
    DEVICE_TYPE: qemu_arm
    LAVA_TAGS: "qemu-arm-32,"
    LAVA_TESTS: "--test-plan lkft-ltp"
    QA_ENVIRONMENT: "qemu-arm-clang"
  needs:
    - build-arm-clang-12
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^linux-4.*/'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-qemu-x86_64-clang-12:
  extends:
    - .test
  variables:
    ARCH: x86
    DEVICE_TYPE: qemu_x86_64
    LAVA_TESTS: "--test-plan lkft-ltp"
    QA_ENVIRONMENT: "qemu-x86_64-clang"
  needs:
    - build-x86-clang-12
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.4.y"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.9.y"'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-4.14.y"'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success

test-qemu-i386-clang-12:
  extends:
    - .test
  variables:
    ARCH: i386
    DEVICE_TYPE: qemu_i386
    LAVA_TESTS: "--test-plan lkft-ltp"
    QA_ENVIRONMENT: "qemu-i386-clang"
  needs:
    - build-i386-clang-12
    - job: download_prerequisites
      artifacts: true
  rules:
    - if: '$RUN_TEST == "0"'
      when: never
    - if: '$CI_COMMIT_BRANCH =~ /^linux-4.*/'
      when: never
    - if: '$CI_COMMIT_BRANCH == "linux-5.4.y"'
      when: never
    - if: '$MANUAL_TEST == "1"'
      when: manual
    - when: on_success
