#!/bin/bash -ex

F_ABS_PATH=$(readlink -e $0)
DIR_PARENT=$(dirname ${F_ABS_PATH})

ANDROID_BUILD_CONFIG="${1}"
if [ -z "${ANDROID_BUILD_CONFIG}" ]; then
    echo "No build config specified!"
    exit 1
fi

if [ -z "${BUILD_TYPE}" ]; then
    BUILD_TYPE="tuxsuite"
fi

if [ "X${BUILD_TYPE}" != "Xtuxsuite" ] && [ "X${BUILD_TYPE}" != "Xjenkins" ]; then
    echo "Unsported BUILD_TYPE specified: ${BUILD_TYPE}"
    exit 1
fi

apt-get update && apt-get install -y wget git virtualenv
####################################################################################
# for tuxsuite build, kernel version is from the kernel build config
# as only the kernel is build, the userspace images are recreated with lava job
# the ANDROID_BUILD_CONFIG here is only used for submitting test jobs
#
# for jenkins build, the vendor build will build the kernel modules,
# recreate the userspace images, and upload them to snapshot
f_build_json="${ANDROID_BUILD_CONFIG}-build.json"
f_misc_info="${ANDROID_BUILD_CONFIG}-misc_info.txt"
if [ "X${BUILD_TYPE}" = "Xtuxsuite" ]; then
    if [ -n "${KERNEL_BUILD_CONFIG}" ]; then
        f_build_json="${KERNEL_BUILD_CONFIG}-build.json"
        f_misc_info="${KERNEL_BUILD_CONFIG}-misc_info.txt"
    fi

    TUXSUITE_DOWNLOAD_URL="$(jq -r '.[] | .download_url' "${f_build_json}")"
    if [ -z "${TUXSUITE_DOWNLOAD_URL}" ]; then
        echo "Failed to get the value for TUXSUITE_DOWNLOAD_URL from the file of ${f_build_json}."
        exit 1
    fi

    SRCREV_kernel="$(jq -r '.[] | .git_sha' "${f_build_json}")"
    KERNEL_VERSION="$(jq -r '.[] | .kernel_version' "${f_build_json}")"
    KERNEL_DESCRIBE=${KERNEL_VERSION}-${SRCREV_kernel:0:12}

    JOB_NAME="${CI_PROJECT_NAME}-${CI_COMMIT_REF_NAME}"
    BUILD_NUMBER="${CI_PIPELINE_ID}"
    BUILD_URL="${CI_PIPELINE_URL}"

elif [ "X${BUILD_TYPE}" = "Xjenkins" ]; then
    JOB_NAME=$(jq .JENKINS_CI_BUILD_NAME "${f_build_json}" |tr -d \")
    BUILD_NUMBER=$(jq .JENKINS_BUILD_NUMBER "${f_build_json}" |tr -d \")
    BUILD_URL=$(jq .JENKINS_BUILD_URL "${f_build_json}" |tr -d \")
    snapshot_download_url=$(jq .SNAPSHOT_DOWNLOAD_URL "${f_build_json}" |tr -d \")
    wget -c "${snapshot_download_url}/${ANDROID_BUILD_CONFIG}-misc_info.txt" -O "${f_misc_info}"

    f_build_kernel="${f_build_json}"
    if [ -n "${KERNEL_BUILD_CONFIG}" ]; then
        f_build_json="${KERNEL_BUILD_CONFIG}-build.json"
    fi
    SRCREV_kernel=$(jq .SRCREV_kernel "${f_build_json}" |tr -d \")
    KERNEL_VERSION=$(jq .MAKE_KERNELVERSION "${f_build_json}" |tr -d \")
    KERNEL_DESCRIBE=$(jq .KERNEL_DESCRIBE "${f_build_json}" |tr -d \")

else
    echo "Unsported BUILD_TYPE specified: ${BUILD_TYPE}"
    exit 1
fi

export LKFT_WORK_DIR="/linaro-android/lkft"
export JOB_NAME
export BUILD_NUMBER
export BUILD_URL
export ANDROID_BUILD_CONFIG
export TUXSUITE_DOWNLOAD_URL
export SRCREV_kernel
export KERNEL_DESCRIBE
#export ENV_DRY_RUN=true

rm -fr "${LKFT_WORK_DIR}" && mkdir -p "${LKFT_WORK_DIR}"

git clone -b lkft https://android-git.linaro.org/git/android-build-configs ${LKFT_WORK_DIR}/android-build-configs

wget -c https://git.linaro.org/ci/job/configs.git/plain/lkft/lava-job-definitions/submit_for_testing-v2.sh -O "${LKFT_WORK_DIR}/submit_for_testing-v2.sh" && chmod +x "${LKFT_WORK_DIR}/submit_for_testing-v2.sh"

# prepare misc_info.txt which is required by submit_for_testing-v2.sh
mkdir -p ${LKFT_WORK_DIR}/out/${ANDROID_BUILD_CONFIG}
cp -v "${f_misc_info}" "${LKFT_WORK_DIR}/out/${ANDROID_BUILD_CONFIG}/misc_info.txt"

pip install virtualenv
virtualenv --python=python3  .venv
source .venv/bin/activate
pip install Jinja2 requests urllib3 ruamel.yaml squad-client

${LKFT_WORK_DIR}/submit_for_testing-v2.sh
