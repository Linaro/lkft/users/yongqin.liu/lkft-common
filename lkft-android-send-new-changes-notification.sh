#!/bin/bash -ex

source ${DIR_SCRIPTS_PATH}/lkft-common-bash-function.sh
######################################################
kernel_version=$(get_kernel_version)
kernel_branch="${CI_BUILD_REF_NAME}"
kernel_label="${kernel_version}-${CI_COMMIT_SHA:0:12}"
# https://gitlab.com/Linaro/lkft/users/yongqin.liu/android-common/-/tree/android-mainline
url_repository="${KERNEL_REPO}/-/tree/${CI_BUILD_REF_NAME}/"

## record the kernel version information for the later stage jobs
## which will be in the artifacts
f_kernel_version_json="${F_KERNEL_INFO}"
cat >"${f_kernel_version_json}" <<__EOF__
{
    "KERNEL_REPOSITORY": "${url_repository}",
    "KERNEL_BRANCH": "${kernel_branch}",
    "SRCREV_kernel": "${CI_COMMIT_SHA}",
    "CI_COMMIT_SHA": "${CI_COMMIT_SHA}",
    "CI_COMMIT_BEFORE_SHA": "${CI_COMMIT_BEFORE_SHA}",
    "KERNEL_DESCRIBE": "${kernel_label}",
    "KERNEL_LABEL": "${kernel_label}",
    "MAKE_KERNELVERSION": "${kernel_version}"
}
__EOF__

# send out the notificaation mail
f_email="notification-new-changes.txt"
rm -fr ${f_email}
echo "Please check the following for the changes information:" > ${f_email}
echo "Repository Link: ${url_repository}" >> "${f_email}"
echo "CI_COMMIT_MESSAGE: ${CI_COMMIT_MESSAGE}" >> "${f_email}"

msg_subject="[lkft-android-report] ${CI_PROJECT_NAME} | ${kernel_branch} | ${kernel_label}"
send_mail_common "${f_email}" "${msg_subject}"
